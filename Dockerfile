FROM pytorch/pytorch:1.7.0-cuda11.0-cudnn8-runtime

## ensure locale is set during build
ENV LANG C.UTF-8

ARG DEBIAN_FRONTEND=noninteractive

RUN python -m pip install --upgrade pip

RUN pip3 install argparse && \
    pip3 install nodejs && \
    pip3 install uproot && \
    pip3 install jupyter && \
    pip3 install jupyterhub && \
    pip3 install jupyterlab && \
    pip3 install matplotlib && \
    pip3 install seaborn && \
    pip3 install hep_ml && \
    pip3 install sklearn && \
    pip3 install scipy && \
    pip3 install tables && \
    pip3 install "dask[complete]" && \
    pip3 install pydot && \
    pip3 install pyparser && \
    pip3 install pyparsing && \
    pip3 install pytest && \
    pip3 install pandas && \
    pip3 install torch-scatter -f https://pytorch-geometric.com/whl/torch-1.7.0+cu110.html && \
    pip3 install torch-sparse -f https://pytorch-geometric.com/whl/torch-1.7.0+cu110.html && \
    pip3 install torch-cluster -f https://pytorch-geometric.com/whl/torch-1.7.0+cu110.html && \
    pip3 install torch-spline-conv -f https://pytorch-geometric.com/whl/torch-1.7.0+cu110.html && \
    pip3 install torch-geometric && \
    pip3 install pyro-ppl 

RUN apt-get update && \
    apt-get install -y git debconf-utils && \
    echo "krb5-config krb5-config/add_servers_realm string CERN.CH" | debconf-set-selections && \
    echo "krb5-config krb5-config/default_realm string CERN.CH" | debconf-set-selections && \
    apt-get install -y krb5-user && \
    apt-get install -y vim less screen graphviz python3-tk wget && \
    apt-get install -y jq tree hdf5-tools bash-completion
